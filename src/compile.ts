import { exec } from 'child_process';
import * as fs from 'fs';
import * as path from 'path';
import { FuzzerConfig } from './config';
import * as parser from '@babel/parser';
import traverse from '@babel/traverse';
import * as generator from '@babel/generator';
import * as t from '@babel/types';

type CompileType = 'pre' | 'post';

const runExecCommand = (command: string): Promise<void> => {
  return new Promise((resolve, reject) => {
    console.log(`Running command: ${command}`);
    exec(command, (error, stdout, _stderr) => {
      if (error) {
        console.error(`Compilation failed: ${error.message}`);
        reject(error);
      } else {
        console.debug('Project compilation successful.');
        console.debug('stdout:', stdout);
        resolve();
      }
    });
  });
};

// Function to run the pre/post compile scripts
export const runCompileScripts = (
  config: FuzzerConfig,
  type: CompileType = 'pre'
): Promise<void> => {
  return new Promise((resolve, reject) => {
    try {
      const compileCommands =
        type === 'pre' ? config.preCompile : config.postCompile;
      if (
        compileCommands &&
        Array.isArray(compileCommands) &&
        compileCommands.length > 0
      ) {
        console.debug(`Running ${type} compile scripts`);
        for (let command of compileCommands) {
          runExecCommand(command);
        }
      }
      resolve();
    } catch (error) {
      reject(error);
    }
  });
};

// Function to compile the underlying project
export const compileProject = async (config: FuzzerConfig): Promise<void> => {
  console.debug(`Compiling TypeScript project`);
  // Use the custom command (if available) or use the default command
  await runExecCommand(
    config.customCompileCommand && config.customCompileCommand !== ''
      ? config.customCompileCommand
      : 'tsc -p .'
  );
};

function findFile(
  startPath: string,
  targetPathSegments: string[]
): string | null {
  // Join the targetPathSegments and check if the file exists
  let targetPath = path.join(startPath, ...targetPathSegments);
  if (fs.existsSync(targetPath)) {
    return targetPath;
  }

  // Check if the file exists in the root of the startPath
  targetPath = path.join(
    startPath,
    targetPathSegments[targetPathSegments.length - 1]
  );
  if (fs.existsSync(targetPath)) {
    return targetPath;
  }

  // Create combinations of the targetPathSegments and try to find a match
  for (let i = 0; i < targetPathSegments.length - 1; i++) {
    for (let j = i + 1; j < targetPathSegments.length; j++) {
      targetPath = path.join(
        startPath,
        ...targetPathSegments.slice(i, j),
        targetPathSegments[targetPathSegments.length - 1]
      );
      if (fs.existsSync(targetPath)) {
        return targetPath;
      }
    }
  }

  // If no match is found, return null
  return null;
}

function getRelativePath(filePath: string): string {
  const absolutePath = path.resolve(filePath);
  const relativePath = path.relative(process.cwd(), absolutePath);
  const depth = relativePath.split(path.sep).length - 1;
  let result = './';

  if (depth > 0) {
    result = '../'.repeat(depth);
  }

  return result;
}

export const refactorFuzzTarget = (target: string, config: FuzzerConfig) => {
  const buildDirectory = config.buildDirectory
    ? config.buildDirectory
    : 'build';
  const fileContents = fs.readFileSync(target, 'utf8');
  const ast = parser.parse(fileContents, {
    sourceType: 'module',
    plugins: ['dynamicImport'],
  });
  traverse(ast, {
    CallExpression(p) {
      if (t.isIdentifier(p.node.callee) && p.node.callee.name === 'require') {
        const arg = p.node.arguments[0];
        if (t.isStringLiteral(arg)) {
          const requirePath = arg.value;
          if (requirePath.startsWith('./') || requirePath.startsWith('../')) {
            let newPath = requirePath.replace(/\.\w+$/, '') + '.js';
            newPath = path.resolve(path.dirname(target), newPath);
            newPath = path.relative(process.cwd(), newPath);
            const targetPathSegments = newPath.split(path.sep);
            const bestMatch = findFile(buildDirectory, targetPathSegments);
            if (bestMatch) {
              arg.value = getRelativePath(target) + bestMatch;
            } else throw new Error(`File not found: ${newPath}`);
          }
        }
      }
    },
  });

  const newFileContents = generator.default(ast).code;
  const newFilePath = path.join(
    path.dirname(target),
    path.basename(target, '.js') + '_new.js'
  );
  fs.writeFileSync(newFilePath, newFileContents);
  return newFilePath;
};
