import * as fs from 'fs';

export interface FuzzerConfig {
  buildDirectory: string;
  preCompile: string[];
  customCompileCommand: string;
  postCompile: string[];
}

export const readConfig = (): FuzzerConfig | null => {
  try {
    const configPath = './.jsfuzzrc';
    const configData = fs.readFileSync(configPath, 'utf-8');
    return JSON.parse(configData) as FuzzerConfig;
  } catch (error) {
    console.error('Error reading the configuration file:', error);
    return null;
  }
}